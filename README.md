# Personal webpage

This is a web page to show my projects and annotations.

See [what it looks like](https://moryama.gitlab.io).

With this project I practiced building a webpage using **React**.

I played around with:

- React **state hook** for state management
- using the **effect hook** to create a moving element
- React **components** hierarchy
- implementation of **sorting** and **filtering**
- a bit of Javascript **spread syntax** and **string manipulation**
- **Bootstrap** integration with React
- Boostrap **grid**
- components **inline-styling**
- **refactoring** as the app grows

## Tools

**Framework:** React 17.0.1

**Routing:** [react-router](https://github.com/ReactTraining/react-router)

**Style:** [react-bootstrap](https://react-bootstrap.github.io/)

**Deployed on:** GitLab Pages

## Development notes

### **Making a flying balloon with React Effect Hook**

On the _Learning_ section of the site, at the bottom of the page, you see the image of a balloon.

When hovering on it, the balloon "flies" to the top of the page.

This is achieved using the React [Effect Hook](https://reactjs.org/docs/hooks-effect.html).

Here's the [code](https://gitlab.com/moryama/moryama.gitlab.io/-/blob/master/src/components/Balloon.js).

#### **Balloon position on page**

One challenge is to set the balloon in a position acceptable for different **screen sizes**. My current solution achieves this in a good number of cases (with known exception of small screens set on vertical view).

The balloon element has 2 position attributes, `left` and `top`.

To adapt these values to different screen sizes, we need to determine them based on the size of the _learning path_ `div` element containing the balloon.

In other words we need to be able to access the `div`'s values _before_ it is rendered, in order to have our balloon ready in position.

To achieve this we can use the **React Effect Hook** via the `useEffect` function because _the function passed to useEffect will run after the render is committed to the screen_ ([source](https://reactjs.org/docs/hooks-reference.html#useeffect)).

Inside the `useEffect` function we can access the `div`'s height and width, make our calculations and then use the State Hook to set and make the `left` and `top` values available to the rest of the component.

#### **Flying up movement**

To create the fly-to-the-top movement, the `top` attribute of the element is changed gradually using `setTimeOut`.

This is achieved again by using the Effect Hook, which lets us perform **side effects** in function components.

```
useEffect(() => {
    if (isFlying) {
      setTimeout(goUp, 3.5);
    }
  }, [isFlying, goUp]);  // Effect dependencies
```

Note how we need to pass a second argument to `useEffect` which is an array of the **dependencies** of our function. This will make the action fire only after a render is caused by `isFlying` or `goUp` states ([source](https://reactjs.org/docs/hooks-reference.html#conditionally-firing-an-effect)).

## Data

Data is stored into a `src/data.json`.

I guess it can't get more casual than that.

## Usage

```
$ git clone https://gitlab.com/moryama/moryama.gitlab.io.git
$ cd moryama.gitlab.io
$ npm install
$ npm start
```

## License

[Creative Commons BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/)
