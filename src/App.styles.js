import styled from 'styled-components';
import { palette } from './constants/palette';

export const AppWrapper = styled.div`
    font-family: "Abel";
    font-size: x-large;
    color: ${palette.font};
    /* Fill whole space on screen */
    width: 100%;
    height: 100%;
    margin: 0px;
    padding: 0px;
    overflow-x: hidden;
    /* Make the footer stick */
    display: flex;
    min-height: 100vh;
    flex-direction: column;
`;