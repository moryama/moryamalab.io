import styled from "styled-components";
import { palette } from "../constants/palette";


export const Title = styled.h1`
    font-size: xxx-large;
    font-weight: 500;
    background-color: ${palette.green.lighter};
    background-position: left;
    border-radius: 5px;
    padding: 3px 5px 3px 5px;
    text-align: center;
`;