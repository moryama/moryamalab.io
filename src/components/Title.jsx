import * as S from './Title.styles';

const Title = ({ onClick, text }) => {
  return <S.Title onClick={onClick}>{text}</S.Title>;
};

export default Title;
