import styled from "styled-components";

export const AboutWrapper = styled.div`
    font-size: 1.8em;
    text-align: right;
`;

export const InfoWrapper = styled.div`
    margin-bottom:  1.2em;
    text-align: justify;
`;