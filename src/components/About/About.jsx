import React from 'react';
import * as S from './About.styles';

const About = () => {
  return (
    <S.AboutWrapper>
      <p>
        My background is in education working with adult learners. Now I&apos;m
        a frontend developer with some backend understanding. I love to build
        with Typescript/React and encourage collaborative programming.
      </p>
    </S.AboutWrapper>
  );
};

export default About;
