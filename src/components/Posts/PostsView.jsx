import React, { useContext } from 'react';
import PostList from './PostList';
import Post from './Post';
import { PostContext } from '../../context/PostContext';
import { Route, Switch } from 'react-router-dom';
import { PATH } from '../../constants/path';

const PostsView = () => {
  const { postToShow } = useContext(PostContext);

  return (
    <div>
      {postToShow ? (
        <Switch>
          <Route path={`${PATH.posts}/${postToShow.title}`}>
            <Post />
          </Route>
        </Switch>
      ) : (
        <PostList />
      )}
    </div>
  );
};

export default PostsView;
