import { LearningPost } from "./LearningPost/LearningPost";
import { ReduxPost } from "./ReduxPost/ReduxPost";

export const posts = [
    {
        title:
            'How I migrated an application from Redux to React Context and hooks',
        keywords: 'planning, refactoring, problem-solving',
        content: <ReduxPost />,
        date: 'January 2023'
    },
    {
        title: 'How I learnt to program',
        keywords: 'tools, courses, learning steps, autonomy',
        content: <LearningPost />,
        date: 'June 2021'
    },
];
