import balloonColorImg from './img/balloonColor.png';
import balloonBWImg from './img/balloonBW.png';

const BalloonImage = ({ handleOnHover, isColored }) => {
  return isColored ? (
    <img
      src={balloonColorImg}
      alt="a hot-air balloon full of colors"
      style={{ height: '230px', marginTop: '0.7em' }}
      onMouseOver={handleOnHover}
    />
  ) : (
    <img
      src={balloonBWImg}
      alt="a hot-air balloon"
      style={{ height: '230px', marginTop: '0.7em' }}
      onMouseOver={handleOnHover}
    />
  );
};

export default BalloonImage;
