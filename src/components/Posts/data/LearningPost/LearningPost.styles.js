import styled from "styled-components";
import { highlights, palette } from "../../../../constants/palette";
import planPattern from './img/pattern.png';
import Badge from 'react-bootstrap/Badge';
import { Tooltip } from "react-bootstrap";

export const LearningPostWrapper = styled.div`
    text-align: justify;

    a {
        text-decoration: underline;
        text-decoration-style: dotted;
        text-decoration-color: ${palette.green.lighter};

        margin-top: 1em;
        color: ${palette.green.darker};
    }

    a:hover {
        text-decoration: underline;
        text-decoration-style: solid;
        text-decoration-color: ${palette.green.darker};
    }
`;

export const FilterButtonStyle = styled.button`
    background: ${palette.green.lighter};
    opacity: 85%;
    border: none;
    margin: 0.3em 0.3em 0.3em 0;
    border-radius: 3px;
`;

export const FilterWidget = styled.div`
    margin-top: 1.3em;
    margin-bottom: 1.3em;
`;

const planStyle = `
    background-image: url(${planPattern});
    padding: 0 10px 0 10px;
    border-radius: 10px;
    background-color: red;
`;

export const StepWrapper = styled.li`
    text-shadow: ${props => props.step.highlighted ? highlights[props.step.type] : 'none'};
    font-size: 1.05em;

    ${props => props.isPlan && planStyle}
`;

export const StepBadge = styled(Badge)`
  font-size: 85% !important;
  border-radius: 3px !important;
  text-shadow: none;
  margin-right: 0.4em;
`;

export const BalloonImageWrapper = styled.div`
    display: flex;
    flex-direction: column;
`;

export const BalloonTooltip = styled(Tooltip)`
    display: ${props => props.$isVisible ? '' : 'none'}
`;
