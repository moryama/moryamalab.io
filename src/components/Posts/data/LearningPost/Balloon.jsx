import React, { useCallback, useEffect, useState } from 'react';
import OverlayTrigger from 'react-bootstrap/OverlayTrigger';
import BalloonImage from './BalloonImage';
import * as S from './LearningPost.styles';

const Balloon = ({ isColored, isHidden }) => {
  if (isHidden) {
    return null;
  }

  const [balloon, setBalloon] = useState({ left: null, top: null });
  const [isFlying, setIsFlying] = useState(false);
  const [isOnTop, setIsOnTop] = useState(false);

  // Calculate the balloon initial position
  useEffect(() => {
    const div = document.getElementById('main-view');
    const divHeight = div.offsetHeight;
    const divWidth = div.offsetWidth;
    const imageHeight = document.getElementById('balloon').offsetHeight;
    const extraSpaceInWidth = divWidth - (divWidth / 100) * 50;

    setBalloon({
      left: divWidth + extraSpaceInWidth,
      top: divHeight - imageHeight,
    });
  }, []);

  const goUp = useCallback(() => {
    const step = 3.7;

    if (balloon.top > 550) {
      setBalloon({ ...balloon, top: balloon.top - step });
    } else {
      setIsFlying(false);
      setTimeout(() => setIsOnTop(true), 1000);
    }
  }, [balloon]);

  // Make balloon go up
  useEffect(() => {
    if (isFlying) {
      setTimeout(goUp, 3.5);
    }
  }, [isFlying, goUp]);

  const handleOnHover = () => {
    return balloon.top > 1 ? setIsFlying(true) : undefined;
  };

  const BalloonStyle = {
    position: 'absolute',
    left: balloon.left,
    top: balloon.top,
    textAlign: 'center',
  };

  return (
    <div id="balloon" style={BalloonStyle}>
      <OverlayTrigger
        placement="top"
        delay={{ show: 10, hide: 350 }}
        overlay={
          <S.BalloonTooltip id="button-tooltip" $isVisible={isOnTop}>
            Colour me! Can you find a way to turn on 4 colours on this page?
          </S.BalloonTooltip>
        }
      >
        <S.BalloonImageWrapper>
          <BalloonImage isColored={isColored} handleOnHover={handleOnHover} />
        </S.BalloonImageWrapper>
      </OverlayTrigger>
    </div>
  );
};

export default Balloon;
