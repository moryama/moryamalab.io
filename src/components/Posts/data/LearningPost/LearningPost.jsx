import data from './data.json';
import Step from './Step';
import * as S from './LearningPost.styles';
import { useState } from 'react';
import Balloon from './Balloon';

export const LearningPost = () => {
  const learning = data.learning;
  const [steps, setSteps] = useState(learning.steps);
  const [isSortedByOldest, setIsSortedByOldest] = useState(false);

  const colorsOnInitialState = {};
  for (const key of learning.types) {
    colorsOnInitialState[key] = false;
  }
  const [colorsOn, setColorsOn] = useState(colorsOnInitialState);

  const sortedByOldest = [...learning.steps].sort((a, b) => {
    return a.id - b.id;
  });

  const toggleSort = () => {
    isSortedByOldest ? setSteps(learning.steps) : setSteps(sortedByOldest);
    setIsSortedByOldest(!isSortedByOldest);
    return undefined;
  };

  const toggleFilter = (type) => {
    if (type === 'RESET') {
      setSteps(
        [...steps].map((step) => {
          return { ...step, highlighted: false };
        })
      );

      // Balloon: reset to no-colors
      const newColorsOn = { ...colorsOn };
      Object.keys(newColorsOn).forEach((key) => {
        newColorsOn[key] = false;
      });
      setColorsOn(newColorsOn);

      return undefined;
    }

    // Balloon: get one step closer or further from colors
    const newColorsOn = { ...colorsOn };
    newColorsOn[type] = !newColorsOn[type];
    setColorsOn(newColorsOn);

    setSteps(
      [...steps].map((step) => {
        if (step.type !== type) {
          return step;
        } else {
          if (step.highlighted) {
            return { ...step, highlighted: false };
          } else {
            return { ...step, highlighted: true };
          }
        }
      })
    );
    return undefined;
  };

  const colorsAreOn = Object.values(colorsOn).every((elem) => elem === true);

  return (
    <>
      <S.LearningPostWrapper>
        <h3>
          In June 2019 I picked up programming as a fun challenge knowing little
          to nothing about it. I enjoyed it so much that I kept going and I
          noted down the steps I took to learn. I like how they show a nice
          progress so here they are.
        </h3>
        <S.FilterWidget>
          <span style={{ marginRight: '0.3em' }}>filter:</span>
          {learning.types.map((type) => (
            <S.FilterButtonStyle key={type} onClick={() => toggleFilter(type)}>
              {type}
            </S.FilterButtonStyle>
          ))}
          <S.FilterButtonStyle onClick={() => toggleFilter('RESET')}>
            RESET
          </S.FilterButtonStyle>
          <br />
          <S.FilterButtonStyle onClick={() => toggleSort()}>
            sort from {isSortedByOldest ? 'latest' : 'oldest'}
          </S.FilterButtonStyle>
        </S.FilterWidget>
        <ul>
          {steps.map((step) => (
            <Step key={step.id} step={step} />
          ))}
        </ul>
      </S.LearningPostWrapper>
      <Balloon isHidden={false} isColored={colorsAreOn} />
    </>
  );
};
