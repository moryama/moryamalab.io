import React from 'react';
import * as S from './LearningPost.styles';

const Step = ({ step }) => {
  const addBadge = (step) => {
    return step.final ? (
      <span>
        <S.StepBadge bg="dark" text="light">
          JUNE 2021
        </S.StepBadge>
      </span>
    ) : step.oldest ? (
      <span>
        <S.StepBadge bg="dark" text="light">
          JUNE 2019
        </S.StepBadge>
      </span>
    ) : null;
  };

  const composeTextWithLink = (step) => {
    if (step.link) {
      const linkIndex = step.text.indexOf(step.link);
      const textBeforeLink = step.text.slice(0, linkIndex);
      const textAfterLink = step.text.slice(linkIndex + step.link.length);
      return (
        <>
          {textBeforeLink}
          <a href={step.url} target="_blank" rel="noopener noreferrer">
            {step.link}
          </a>
          {textAfterLink}
        </>
      );
    }
  };

  const isPlan = step.type === 'plan';

  return (
    <S.StepWrapper step={step} isPlan={isPlan}>
      {step.link ? composeTextWithLink(step) : step.text}
      <span> {addBadge(step)}</span>
    </S.StepWrapper>
  );
};

export default Step;
