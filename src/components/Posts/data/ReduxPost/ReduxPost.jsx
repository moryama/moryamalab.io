import React from 'react';
import { OverlayTrigger } from 'react-bootstrap';
import { getPopover } from './getPopover';
import initialStatePlanImage from './img/initialStatePlan.jpg';
import newStatePlanImage from './img/newStatePlan.jpg';
import legoImage from './img/lego.jpg';
import * as S from './ReduxPost.styles';

export const ReduxPost = () => {
  return (
    <>
      <br />
      <h3>
        <b>Note: </b>
      </h3>
      <p>
        This may sound like a tutorial but it isn&apos;t.
        <br />
        It’s a list of the steps I took to complete the project.
        <br />
        The goal is to describe the approach I followed.
      </p>
      <br />
      <h2>WHAT</h2>
      <p>
        I chose an old personal project from a couple of years ago.
        <br />
        The project used the Redux library to manage the application state.
        <br />I decided to remove this library and introduce instead a pattern
        of React Context and state hooks.
      </p>
      <br />
      <h2>WHY</h2>
      <p>
        I wanted to learn more about React Context.
        <br />I was curious to compare the performance before and after the
        migration.
        <br />
        Also, I like the gratification of removing a dependency from my projects
        ✨
      </p>
      <br />
      <h2>WHEN</h2>
      <p>
        I did this during a learning period between jobs, over the course of a
        week.
      </p>
      <br />
      <h2>STEPS I TOOK</h2>
      <S.StepTitle>AKNOWLEDGE PRECONCEPTIONS</S.StepTitle>
      <p>
        I approached this project with some preconceptions and I noted them
        down:
        <li>Context is like a box to store state</li>
        <li>Context is used for state that is shared across the app</li>
        <li>Context causes re-renders in all consumer components</li>
        <li>
          overall Redux does a great job for my app and migrating will slow it
          down
        </li>
      </p>
      <S.StepTitle>RESEARCH</S.StepTitle>
      <p>
        I started by doing some research online to gather people’s thoughts:
        <li>in general we all agree no tool is better than the other</li>
        <li>many are wary of Context vs Redux performance-wise</li>
        <li>a lot suggest memoization as a fix for the re-renders issue</li>
        <li>React documentation warns about abusing memoization</li>
      </p>
      <S.StepTitle>RE-ASSESS</S.StepTitle>
      <p>
        At this point I was feeling a bit skeptical.
        <br />
        I wondered if the migration could significantly reduce the app’s
        performance.
        <br />
        At the same time I was curious to experiment with this.
        <br />
        It sounded like a good learning opportunity no matter the outcome.
        <br />
        So I moved on.
      </p>

      <S.StepTitle>OBSERVE THE CURRENT STATUS</S.StepTitle>
      <p>
        I made a plan of the{' '}
        <OverlayTrigger
          trigger={['hover', 'focus']}
          placement="bottom"
          overlay={getPopover(
            'initial state plan popover',
            'a diagram of the connections between state and components',
            initialStatePlanImage,
            'something like this'
          )}
        >
          <S.TextWithPopover>initial app state</S.TextWithPopover>
        </OverlayTrigger>
        .
        <br />
        It didn’t take long given the app size.
        <br />
        This helped me visualise which components used and updated each state.
      </p>
      <S.StepTitle>REFACTOR TO CLARIFY</S.StepTitle>
      <p>
        I noticed that the purpose of some state was unclear at first sight.
        <br />
        So I refactored it, mainly by choosing more descriptive and unified
        naming.
      </p>
      <S.StepTitle>MAKE A PLAN</S.StepTitle>
      <p>
        Next I reflected on the initial state plan I had made. <br />I{' '}
        <OverlayTrigger
          trigger={['hover', 'focus']}
          placement="bottom"
          overlay={getPopover(
            'new state plan popover',
            'a diagram of the connections between state and components',
            newStatePlanImage,
            'like so'
          )}
        >
          <S.TextWithPopover>sketched down</S.TextWithPopover>
        </OverlayTrigger>{' '}
        options for the new one.
      </p>
      <S.StepTitle>PREPARE FOR ASSESSMENT</S.StepTitle>
      <p>
        While making the plan I realised I missed a way to compare the
        performance of the &lsquo;before&lsquo; and &lsquo;after&lsquo;. <br />
        To solve this I looked into React Dev Tools.
        <br />I learnt how to visually highlight which components are rendered
        at any moment.
        <br />I also learnt how to use the Profiler tool to pinpoint the reason
        for a component rendering.
      </p>
      <S.StepTitle>REFACTOR TO ORGANISE</S.StepTitle>
      <p>
        Before starting the migration I wanted to do some preparatory
        refactoring.
        <br />I broke down and reorganised some components.
        <br />
        This increased logic isolation and limited the re-renderings.
        <br />
        It was a good moment to practice the new React Dev Tools I had just
        learnt.
      </p>
      <S.StepTitle>ACT IN SMALL STEPS</S.StepTitle>
      <p>
        After the clean-up I went on to the actual migration.
        <br />I decided to begin with the parts I perceived as easier and move
        up towards the trickiest.
        <br />
        Unsurprisingly some easy parts ended up being challenging, and vice
        versa.
      </p>
      <S.StepTitle>PROBLEM SOLVING</S.StepTitle>
      <p>
        During the whole process I referenced the new React beta documentation.
        <br />
        It includes effective tutorials with patterns and good practices.
      </p>
      <p>
        In addition I used Legos to{' '}
        <OverlayTrigger
          trigger={['hover', 'focus']}
          placement="bottom"
          overlay={getPopover(
            'lego model popover',
            'a model built with legos',
            legoImage,
            'a thingy like that'
          )}
        >
          <S.TextWithPopover>build a model</S.TextWithPopover>
        </OverlayTrigger>{' '}
        of the architecture
        <i> components-state-hooks-context</i>.
        <br />
        This helped me understand the workflow and test different approaches.
        <br />
        It also added a space for creativity and mind-roaming into my work day.
        <br />
        This can facilitate problem-solving and elaborating ideas.
      </p>
      <S.StepTitle>REVISIT INITIAL PRECONCEPTIONS</S.StepTitle>
      <p>
        Finally, after the migration I revisited the preconceptions I had at the
        beginning of the project.
      </p>
      <ul>
        <li>
          <i>Context is like a box to store state</i>
        </li>
        <ul>
          <li>
            not quite so, now I see Context more as a way to set state in a
            parent component and ‘teleport’ it anywhere is needed
          </li>
        </ul>
        <li>
          <i>Context is used for state shared across the app</i>
        </li>
        <ul>
          <li>
            this is true, although in some cases it can be ok - or even a plus -
            to just pass down props
          </li>
        </ul>
        <li>
          <i>Context causes re-renders in all consumer components</i>
        </li>
        <ul>
          <li>
            this is true and you should choose carefully where to place the
            provider
          </li>
          <li>
            one way to minimise issues is having <i>one</i> piece of state per
            <i> one</i> context
          </li>
          <li>
            I’d use memo sparingly e.g. in case of a specific action that is
            inherently expensive
          </li>
        </ul>
        <li>
          <i>
            overall Redux does a great job and migrating will slow down the app
          </i>
        </li>
        <ul>
          <li>
            Redux did a great job for sure but the app didn’t slow down with the
            new pattern of Context and hooks. I wonder how much the small size
            of the app contributes to this
          </li>
        </ul>
      </ul>
      <S.StepTitle>DRAW SOME CONCLUSIONS</S.StepTitle>
      <p>
        Using Redux when I initially built the app was a good choice. <br />
        It worked well and I learnt new concepts through it, for example what{' '}
        <i> boilerplate code</i> involves.
        <br />
        It also did a lot of things for me out of the box so I didn’t need to
        ruminate too much.
        <br />
        Doing this migration pushed me to reflect and better appreciate how
        React works.
      </p>
      <p>
        When I’ll start my next React app I’ll choose the Context + hooks
        pattern.
        <br />
        At some stage of the app development it could make sense to introduce a
        tool like Redux.
        <br />
        It will be interesting to grow the application and find out the breaking
        point.
      </p>
      <S.StepTitle>TAKE NOTES</S.StepTitle>
      <p>
        Writing these notes helped me to understand and summarise the project
        outcome and its value.
      </p>
    </>
  );
};
