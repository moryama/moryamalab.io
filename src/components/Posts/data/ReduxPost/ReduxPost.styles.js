import styled from "styled-components";
import { palette } from "../../../../constants/palette";
import { Image as BSImage, Popover as BSPopover } from 'react-bootstrap';

export const TextWithPopover = styled.span`
    background-color: ${palette.green.lighterTransparent};
    border-radius: 3px;
    padding: 0 3px 0 3px;
`;

export const PopoverWrapper = styled(BSPopover)`
    min-width: 700px;
`;

export const PopoverHeader = styled(BSPopover.Header)`
    background-color: ${palette.green.lighterTransparent};
    padding: 10px;
    font-family: Abel;
    font-size: large;
    text-align: center;
`;

export const PopoverImage = styled(BSImage)`
    padding: 15px
`;

export const StepTitle = styled.h4`
    margin: 25px 0 10px 0;

    text-decoration: underline 8px;
    text-decoration-color: ${palette.green.lighter};
`;