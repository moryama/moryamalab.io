import React from 'react';
import * as S from './ReduxPost.styles';

export const getPopover = (id, imageAlt, imageSrc, text) => {
  return (
    <S.PopoverWrapper id={id}>
      <S.PopoverHeader as="p">{text}</S.PopoverHeader>
      <S.PopoverImage fluid src={imageSrc} alt={imageAlt} />
    </S.PopoverWrapper>
  );
};
