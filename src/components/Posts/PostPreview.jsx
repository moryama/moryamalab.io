import React, { useContext } from 'react';
import { PostContext } from '../../context/PostContext';
import styled from 'styled-components';
import Title from '../Title';
import { Link } from 'react-router-dom';
import { PATH } from '../../constants/path';
import { palette } from '../../constants/palette';

const PostTitle = styled(Link)`
  :hover {
    h1 {
      background-color: ${palette.green.lighterTransparent};
    }
    text-decoration: none;
  }
`;

const PostPreviewWrapper = styled.div`
  margin-bottom: 40px;
  a {
    color: ${palette.font};
  }
`;

const PostPreview = ({ post }) => {
  const { setPostToShow } = useContext(PostContext);
  return (
    <PostPreviewWrapper key={post.title}>
      <PostTitle to={`${PATH.posts}/${post.title}`}>
        <Title text={post.title} onClick={() => setPostToShow(post)} />
      </PostTitle>
      <h5>{post.date}</h5>
      <h3>{post.keywords}</h3>
    </PostPreviewWrapper>
  );
};

export default PostPreview;
