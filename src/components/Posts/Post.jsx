import React, { useContext } from 'react';
import { Link } from 'react-router-dom';
import { PATH } from '../../constants/path';
import { initialState, PostContext } from '../../context/PostContext';
import Title from '../Title';
import * as S from './Post.styles';

const BackButton = ({ onClick }) => {
  return (
    <Link to={PATH.posts}>
      <S.BackButton onClick={() => onClick()}>back</S.BackButton>
    </Link>
  );
};

const Post = () => {
  const { postToShow, setPostToShow } = useContext(PostContext);
  const handleBackButton = () => {
    setPostToShow(initialState);
  };

  return (
    <S.PostWrapper>
      <BackButton onClick={handleBackButton} />
      <Title text={postToShow.title} />
      {postToShow.content}
      <BackButton onClick={handleBackButton} />
    </S.PostWrapper>
  );
};

export default Post;
