import React from 'react';
import { posts } from './data/posts';
import PostPreview from './PostPreview';

const PostList = () => {
  return (
    <>
      {posts.map((post) => {
        return <PostPreview key={post.title} post={post} />;
      })}
    </>
  );
};

export default PostList;
