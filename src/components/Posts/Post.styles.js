import styled from "styled-components";
import { palette } from "../../constants/palette";

export const PostWrapper = styled.div`
    margin-bottom: 80px;
`;

export const BackButton = styled.button`
    margin: 30px 0 30px 0;
    background-color: ${palette.green.light};
    color: white;
    font-weight: 500;
    border: none;
    border-radius: 5px;

    :hover {
        background-color: ${palette.green.lighter};
    }
`;