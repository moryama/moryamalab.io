import React from 'react';
import { OverlayTrigger, Tooltip } from 'react-bootstrap';
import data from './data.json';
import * as S from './Footer.styles';

const Footer = () => {
  const currYear = new Date().getFullYear();
  return (
    <>
      <S.FooterWrapper>
        <S.FooterContent>
          {currYear}
          <span> / </span>
          <OverlayTrigger
            placement="top"
            overlay={
              <Tooltip>
                <div>Do you have my work email? Great, use that 👉</div>
                <div>
                  Otherwise 👋 hi stranger!
                  <br />
                  click below
                </div>
              </Tooltip>
            }
          >
            <a href={data.footer.mail}>Contact me</a>
          </OverlayTrigger>
          <span> / </span>
          <a
            href={data.footer.gitlab}
            target="_blank"
            rel="noopener noreferrer"
          >
            GitLab
          </a>
          <span> / </span>
          <a
            href={data.footer.stickyFooter}
            target="_blank"
            rel="noopener noreferrer"
          >
            How does this footer stick?
          </a>
        </S.FooterContent>
      </S.FooterWrapper>
    </>
  );
};

export default Footer;
