import styled from 'styled-components';
import { palette } from '../../constants/palette';

export const FooterWrapper = styled.footer`
    background-color: ${palette.green.dark};
    text-align: center;
    height: 5em;
    opacity: 80%;
    color: white;

    a {
        color: white
    }
`;

export const FooterContent = styled.div`
    position: relative;
    top: 50%;
    transform: translateY(-50%);
    font-size: 1.3em;
`;
