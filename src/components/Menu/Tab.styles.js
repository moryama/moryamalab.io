import { Link } from "react-router-dom";
import styled from "styled-components";
import { palette } from "../../constants/palette";

const selectedTabColor = '#96AFA6';

const selectedTabStyle = `
  color: white;
  background-color: ${selectedTabColor};
  border: 3px ${selectedTabColor} solid;
  border-radius: 10px 10px 0 0;    
  border-bottom: none;
`;

export const Tab = styled(Link)`
  margin-right: 0.5em;
  font-size: 1.3em;
  font-weight: 500;
  padding: 0 5px 0 5px;
  color: ${palette.font};
  border: 3px white solid;
  border-radius: 10px 10px 0 0;    
  border-bottom: none;
  transition-duration: 0.4s;
  
  &:hover {
    color: black;
    text-decoration: none;
    ${selectedTabStyle}
  }  
  
  ${props => props.$isSelected && selectedTabStyle}
`;