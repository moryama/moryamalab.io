import Nav from 'react-bootstrap/Nav';
import * as S from './Menu.styles';
import Tab from './Tab';
import { PATH } from '../../constants/path';

const Menu = () => {
  return (
    <S.NavbarWrapper>
      <Nav>
        <Tab path={PATH.about} label="About me" />
        <Tab path={PATH.projects} label="Examples of my code" />
        <Tab path={PATH.posts} label="Examples of how I work" />
      </Nav>
    </S.NavbarWrapper>
  );
};

export default Menu;
