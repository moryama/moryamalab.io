import React, { useContext } from 'react';
import { useLocation } from 'react-router-dom';
import * as S from './Tab.styles';
import { initialState, PostContext } from '../../context/PostContext';

const Tab = ({ path, label }) => {
  const { setPostToShow } = useContext(PostContext);

  const location = useLocation();
  const selectedTab = location.pathname;

  return (
    <S.Tab
      to={path}
      $isSelected={selectedTab.includes(path)}
      onClick={() => setPostToShow(initialState)}
    >
      {label}
    </S.Tab>
  );
};

export default Tab;
