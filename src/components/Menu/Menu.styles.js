import styled from "styled-components";
import Navbar from 'react-bootstrap/Navbar';

export const NavbarWrapper = styled(Navbar)`
  margin-top: 27px;
  margin-left: 40px;
  margin-bottom: 100px;
`;