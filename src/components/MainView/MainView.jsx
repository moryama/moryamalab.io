import React from 'react';
import { Route, Switch } from 'react-router-dom';
import About from '../About/About';
import PostsView from '../Posts/PostsView';
import ProjectsView from '../Projects/ProjectsView';
import * as S from './MainView.styles';
import { PATH } from '../../constants/path';

const MainView = () => {
  return (
    <S.MainViewWrapper id="main-view">
      <Switch>
        <Route exact path={PATH.about}>
          <About />
        </Route>
        <Route path={PATH.projects}>
          <ProjectsView />
        </Route>
        <Route path={PATH.posts}>
          <PostsView />
        </Route>
      </Switch>
    </S.MainViewWrapper>
  );
};

export default MainView;
