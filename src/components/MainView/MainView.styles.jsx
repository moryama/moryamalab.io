import { Container } from 'react-bootstrap';
import styled from 'styled-components';

export const MainViewWrapper = styled(Container)`
  display: flex;
  justify-content: center;
  max-width: 50%;
  /* Make the footer stick */
  flex: 1;
`;
