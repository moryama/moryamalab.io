import React from 'react';
import * as S from './Project.styles';
import Repository from './Repository/Repository';
import { v4 as uuid } from 'uuid';
import Title from '../Title';

const Project = ({ project }) => {
  return (
    <S.ProjectWrapper>
      <Title text={project.title} />
      {project.repositories.map((repository) => {
        return <Repository key={uuid()} repository={repository} />;
      })}
    </S.ProjectWrapper>
  );
};

export default Project;
