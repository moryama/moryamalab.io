import styled from "styled-components";
import { Tooltip as BSTooltip } from 'react-bootstrap';
import { palette } from "../../../constants/palette";

export const Tooltip = styled(BSTooltip)`
    padding-left: 4px;
`;

export const IconWrapper = styled.span`
    :hover {
        background-color: ${props => props.isLink ? palette.green.dark : ''};
        border-radius: 100px;
        
        svg {
            color: ${props => props.isLink ? 'white' : ''} !important;
        }
    }
`;
