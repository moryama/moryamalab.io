import styled from "styled-components";

export const RepositoryWrapper = styled.div`
    margin-bottom: 30px;
    margin-top: ${props => props.hasMarginTop ? '50px' : ''};
`;

export const RepositoryTop = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: baseline;
`;

export const ProjectSubtitle = styled.h2`
    margin-bottom: 20px;
`;