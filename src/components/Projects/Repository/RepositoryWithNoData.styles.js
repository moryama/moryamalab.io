import styled from "styled-components";

export const Wrapper = styled.div`
    border: solid 2px #C4D0CC;
    border-radius: 5px;
    padding: 15px;
    margin-bottom: 10px;
`;