import { formatDistanceToNow, parseISO } from 'date-fns';
import Icon from './Icon';
import CommitDate from './CommitDate';
import * as S from './RepositoryData.styles';

const RepositoryData = ({ commitDate, pipelineStatus, url }) => {
  const formattedDate = commitDate && formatDistanceToNow(parseISO(commitDate));
  const pipelineStatusLabel = pipelineStatus && pipelineStatus.toLowerCase();

  return (
    <S.RepositoryDataWrapper>
      {url && (
        <span>
          code{' '}
          <a href={url} target="_blank" rel="noreferrer">
            <Icon iconType="gitlabLink" tooltipText="GitLab" />
          </a>
        </span>
      )}
      {pipelineStatus && (
        <span>
          pipeline status{' '}
          <Icon
            iconType={
              pipelineStatus === 'SUCCESS' || pipelineStatus === 'FAILED'
                ? pipelineStatusLabel
                : 'other'
            }
            tooltipText={pipelineStatusLabel}
          />
        </span>
      )}
      {commitDate && (
        <span>
          {' '}
          last commit <CommitDate date={formattedDate} />{' '}
        </span>
      )}
    </S.RepositoryDataWrapper>
  );
};

export default RepositoryData;
