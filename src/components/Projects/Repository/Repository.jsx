import { useQuery } from 'urql';
import React from 'react';
import { REPOSITORY_QUERY } from '../../../graphql/queries';
import { unpackRepositoryData } from '../../../graphql/unpackRepositoryData';
import Keywords from './Keywords/Keywords';
import * as S from './Repository.styles';
import RepositoryWithNoData from './RepositoryWithNoData';
import CenteredSpinner from './Spinner';
import RepositoryData from './RepositoryData';

const Repository = ({ repository }) => {
  const [result] = useQuery({
    query: REPOSITORY_QUERY,
    variables: {
      fullPath: repository.gitlabFullPath,
    },
  });

  const { data, fetching, error } = result;
  if (fetching) return <CenteredSpinner />;
  if (error) console.log('🙈 This error occurred: ', error);

  const url = `https://gitlab.com/${repository.gitlabFullPath}`;

  if (!data) {
    return <RepositoryWithNoData url={url} />;
  }

  const hasTitle = repository.title !== undefined;

  const [keywords, latestCommitDate, pipelineStatus] =
    unpackRepositoryData(data);

  return (
    <S.RepositoryWrapper hasMarginTop={hasTitle}>
      <S.RepositoryTop>
        <S.ProjectSubtitle>
          {hasTitle ? repository.title : null}
        </S.ProjectSubtitle>
        <RepositoryData
          commitDate={latestCommitDate}
          pipelineStatus={pipelineStatus}
          url={url}
        />
      </S.RepositoryTop>
      {keywords && <Keywords keywords={keywords} />}
    </S.RepositoryWrapper>
  );
};

export default Repository;
