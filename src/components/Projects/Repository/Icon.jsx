import { OverlayTrigger } from 'react-bootstrap';
import {
  IoLogoGitlab,
  IoCheckmarkCircleSharp,
  IoCloseCircleSharp,
  IoReloadCircleSharp,
} from 'react-icons/io5';
import * as S from './Icon.styles';

const icons = {
  gitlabLink: (
    <IoLogoGitlab
      id="gilabLogo"
      color="#96AFA6"
      size="1.2em"
      style={{ marginBottom: '5px' }}
    />
  ),
  success: (
    <IoCheckmarkCircleSharp
      color="green"
      size="1.2em"
      style={{ marginBottom: '4px', opacity: '55%' }}
    />
  ),
  failed: (
    <IoCloseCircleSharp
      color="red"
      size="1.2em"
      style={{ marginBottom: '4px', opacity: '55%' }}
    />
  ),
  other: (
    <IoReloadCircleSharp
      color="#16AAAA"
      size="1.2em"
      style={{ marginBottom: '4px', opacity: '55%' }}
    />
  ),
};

const Icon = ({ iconType, tooltipText }) => {
  const iconComponent = icons[iconType];
  const isLink = iconType === 'gitlabLink';

  return (
    <OverlayTrigger
      placement="right"
      overlay={<S.Tooltip id="icon-tooltip">{tooltipText}</S.Tooltip>}
    >
      <S.IconWrapper isLink={isLink}>{iconComponent}</S.IconWrapper>
    </OverlayTrigger>
  );
};

export default Icon;
