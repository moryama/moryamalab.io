import styled from "styled-components";

export const RepositoryDataWrapper = styled.div`
    display: flex;
    flex-direction: column;
    align-items: flex-end;
    padding-bottom: 15px;
    font-size: 0.8em;
`;