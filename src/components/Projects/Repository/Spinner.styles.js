import { Spinner as BSSpinner } from 'react-bootstrap';
import styled from 'styled-components';
import { palette } from '../../../constants/palette';

export const SpinnerWrapper = styled.div`
  display: flex;
  justify-content: center;
  margin-bottom: 50px;
  margin-top: 50px;
`;

export const Spinner = styled(BSSpinner)`
  background-color: ${palette.green.light};
`;