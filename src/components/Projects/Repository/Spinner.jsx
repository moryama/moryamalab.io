import * as S from './Spinner.styles';

const CenteredSpinner = () => {
  return (
    <S.SpinnerWrapper>
      <S.Spinner animation="grow" role="status">
        <span className="sr-only">Loading...</span>
      </S.Spinner>
    </S.SpinnerWrapper>
  );
};

export default CenteredSpinner;
