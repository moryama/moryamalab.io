import styled from "styled-components";
import { palette } from "../../../../constants/palette";

export const KeywordsWrapper = styled.div`
    display: flex;
    flex-wrap: wrap;
    border: solid 2px ${palette.gray.light};
    gap: 15px;
    border-radius: 5px;
    padding: 15px;
`;

export const Keyword = styled.div`
    background-color: ${palette.green.lighterTransparent};
    border-radius: 5px;
    padding: 3px 5px 3px 5px;
    font-size: large;
`;