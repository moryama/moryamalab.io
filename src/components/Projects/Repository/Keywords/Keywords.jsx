import * as S from './Keywords.styles';

const Keywords = ({ keywords }) => {
  if (keywords.length < 1) {
    return null;
  }

  return (
    <S.KeywordsWrapper>
      {keywords.map((keyword) => {
        return <S.Keyword key={keyword}>{keyword}</S.Keyword>;
      })}
    </S.KeywordsWrapper>
  );
};

export default Keywords;
