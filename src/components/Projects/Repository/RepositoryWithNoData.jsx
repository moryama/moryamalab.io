import * as S from './RepositoryWithNoData.styles';

const RepositoryWithNoData = ({ url }) => {
  return (
    <S.Wrapper>
      <p>🙈 Oops. Some project info should be here but is not.</p>
      <p>
        Please check out this project on
        <a href={url} target="_blank" rel="noreferrer">
          {' '}
          GitLab.
        </a>
      </p>
    </S.Wrapper>
  );
};

export default RepositoryWithNoData;
