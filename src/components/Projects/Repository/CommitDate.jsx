import styled from 'styled-components';
import { palette } from '../../../constants/palette';

const StyledCommitDate = styled.span`
  background-color: ${palette.gray.dark};
  border-radius: 5px;
  padding: 0 3px 0 3px;
  color: white;
  font-size: 0.9em;
`;

const CommitDate = ({ date }) => {
  return <StyledCommitDate>{date} ago</StyledCommitDate>;
};

export default CommitDate;
