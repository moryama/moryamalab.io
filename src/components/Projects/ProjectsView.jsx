import React from 'react';
import data from './data.json';
import Project from './Project';
import * as S from './ProjectsView.styles';

const ProjectsView = () => {
  return (
    <S.ProjectsViewWrapper>
      {data.projects.map((project) => (
        <Project key={project.title} project={project} />
      ))}
    </S.ProjectsViewWrapper>
  );
};

export default ProjectsView;
