module.exports = {
    env: {
        browser: true,
        es6: true,
        node: true,
    },
    extends: ["eslint:recommended", "plugin:react/recommended"],
    parserOptions: {
        ecmaFeatures: {
            jsx: true,
        },
        ecmaVersion: 2018,
        sourceType: "module",
    },
    plugins: ["react"],
    settings: {
        react: {
            version: "detect",
        },
    },
    root: true,
    rules: {
        eqeqeq: "error",
        "no-trailing-spaces": "error",
        "no-console": 0,
        "react/react-in-jsx-scope": 0,
        "object-curly-spacing": ["error", "always"],
        "arrow-spacing": ["error", { before: true, after: true }],
        "linebreak-style": ["error", "unix"],
        semi: ["error", "always"],
        "react/prop-types": 0
    },
};
