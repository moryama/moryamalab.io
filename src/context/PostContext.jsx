import { createContext, useState } from 'react';

export const initialState = undefined;

export const PostContext = createContext();

export const PostContextProvider = ({ children }) => {
  const [postToShow, setPostToShow] = useState(initialState);
  return (
    <PostContext.Provider value={{ postToShow, setPostToShow }}>
      {children}
    </PostContext.Provider>
  );
};

export default { PostContext };
