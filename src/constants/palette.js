export const palette = {
  font: '#444848',
  blue: { bright: '#B3EEFF' },
  green: {
    bright: '#C8F69B',
    darker: '#4A7F6E',
    dark: '#94ACA4',
    light: '#96AFA6',
    lighter: '#C4D0CC',
    lighterTransparent: 'rgba(196, 208, 204, 0.6)'
  },
  gray: { dark: '#a8b3af', light: '#D7DBDA' },
  orange: { bright: '#FFCBA5' },
  yellow: { bright: '#FFEEA5' },
};


const yellow = palette.yellow.bright;
const green = palette.green.bright;
const blue = palette.blue.bright;
const orange = palette.orange.bright;

export const highlights = {
  projects:
    `-3px 0px 3px ${yellow}, 3px 0px 3px ${yellow}, 6px 0px 6px ${yellow}, -6px 0px 6px ${yellow}`,
  courses:
    `-3px 0px 3px ${green}, 3px 0px 3px ${green}, 6px 0px 6px ${green}, -6px 0px 6px ${green}`,
  buildups:
    `-3px 0px 3px ${blue}, 3px 0px 3px ${blue}, 6px 0px 6px ${blue}, -6px 0px 6px ${blue}`,
  tools:
    `-3px 0px 3px ${orange}, 3px 0px 3px ${orange}, 6px 0px 6px ${orange}, -6px 0px 6px ${orange}`,
};
