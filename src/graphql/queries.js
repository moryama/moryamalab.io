export const REPOSITORY_QUERY = `query getProjectByFullPath($fullPath: ID!) {
    project(fullPath: $fullPath) {
      topics
      pipelines(first: 1) {
        edges {
          node {
            status
          }
        }
      }
      repository {
        paginatedTree {
          edges {
            node {
              lastCommit {
                authoredDate
              }
            }
          }
        }
      }
    }
  }
  `;
