export const unpackRepositoryData = (data) => {
    if (!data) {
        return;
    }

    const repository = data.project;

    const keywords = repository.topics;

    const latestCommitDate =
        repository.repository.paginatedTree.edges[0].node.lastCommit.authoredDate;

    const pipelineStatus =
        repository.pipelines.edges[0] &&
        repository.pipelines.edges[0].node.status;

    return [keywords, latestCommitDate, pipelineStatus];
};