import React from 'react';
import { HashRouter } from 'react-router-dom';
import Menu from './components/Menu/Menu';
import Footer from './components/Footer/Footer';
import MainView from './components/MainView/MainView';
import * as S from './App.styles';
import { PostContextProvider } from './context/PostContext';
import { createClient, Provider as GraphQLProvider } from 'urql';

const client = createClient({
  url: 'https://gitlab.com/api/graphql',
  requestPolicy: 'cache-first',
});

const App = () => {
  return (
    <HashRouter>
      <GraphQLProvider value={client}>
        <PostContextProvider>
          <S.AppWrapper>
            <Menu />
            <MainView />
            <Footer />
          </S.AppWrapper>
        </PostContextProvider>
      </GraphQLProvider>
    </HashRouter>
  );
};

export default App;
